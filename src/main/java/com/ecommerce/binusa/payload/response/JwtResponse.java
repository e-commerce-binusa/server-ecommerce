package com.ecommerce.binusa.payload.response;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private final long userId;
    private final String role;

    public JwtResponse(String jwttoken, long userId, String role) {
        this.jwttoken = jwttoken;
        this.userId = userId;
        this.role = role;
    }

    public String getJwttoken() {
        return jwttoken;
    }

    public long getUserId() {
        return userId;
    }

    public String getRole() {
        return role;
    }
}