package com.ecommerce.binusa.controller;


import com.ecommerce.binusa.dto.cartDto.AddToCartDto;
import com.ecommerce.binusa.dto.cartDto.CartDto;
import com.ecommerce.binusa.model.Cart;
import com.ecommerce.binusa.model.Product;
import com.ecommerce.binusa.service.CartService;
import com.ecommerce.binusa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    private CartService cartService;
    @Autowired
    private ProductService productService;

    @GetMapping(path = "/list")
    public ResponseEntity<?> cartList(Authentication authentication) throws Exception {
        CartDto cart = cartService.listCart(authentication);
        return new ResponseEntity<>(cart, HttpStatus.OK);
    }

    @GetMapping(path = "/search")
    public ResponseEntity<?> searchCart(@RequestParam("product") String product, @RequestParam("user") String user) {
        List<Cart> carts = cartService.searchCart(product, user);
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<?> getAll() {
        List<Cart> carts = cartService.getAll();
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }

    @PostMapping(path = "/add", consumes = "application/json")
    public ResponseEntity<?> addToCart(@RequestBody AddToCartDto addToCartDto, Authentication authentication) throws Exception {
        Product product = productService.getProductById(addToCartDto.getProductId());
        Cart cart = cartService.addToCart(addToCartDto, product, authentication);
        return new ResponseEntity<>(cart, HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}", consumes = "application/json")
    public ResponseEntity<?> updateCart(@PathVariable("id") Long id, @RequestBody Cart cart, Authentication authentication) throws Exception {
        Cart carts = cartService.updateCart(id, cart, authentication);
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> deleteCart(@PathVariable("id") Long id, Authentication authentication) throws Exception {
        Cart carts = cartService.deleteCartItem(id, authentication);
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<?> deleteCart(Authentication authentication) {
        List<Cart> carts = cartService.deleteAllCart(authentication);
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }
}
