package com.ecommerce.binusa.controller;

import com.ecommerce.binusa.model.Order;
import com.ecommerce.binusa.service.OrderService;
import com.ecommerce.binusa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @PostMapping(path = "/add", consumes = "application/json")
    public ResponseEntity<?> addOrder(@RequestBody List<Order> order, Authentication authentication) {
        List<Order> orders = orderService.addOrder(order, authentication);
        return new ResponseEntity<>(orders, HttpStatus.CREATED);
    }

    @GetMapping(path = "/user/list")
    public ResponseEntity<?> orderList(Authentication authentication) {
        List<Order> orders = orderService.userOrder(authentication);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<?> orderListItem() {
        List<Order> orders = orderService.allOrder();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/list/{id}")
    public ResponseEntity<?> orderListItem(@PathVariable("id") Long id) {
        Order order = orderService.orderId(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping(path = "/process/{id}")
    public ResponseEntity<?> orderProcess(@PathVariable("id") Long id) {
        Order order = orderService.orderProcess(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
