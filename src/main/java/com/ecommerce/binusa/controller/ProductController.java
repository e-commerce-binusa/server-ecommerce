package com.ecommerce.binusa.controller;

import com.ecommerce.binusa.dto.productDto.AddProductDto;
import com.ecommerce.binusa.model.Cart;
import com.ecommerce.binusa.model.Category;
import com.ecommerce.binusa.model.Product;
import com.ecommerce.binusa.service.CategoryService;
import com.ecommerce.binusa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping(path = "/all")
    public ResponseEntity<?> getAllProduct() {
        List<Product> products = productService.getAllProduct();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(path = "/id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") Long id) throws Exception {
        Product product = productService.getProductById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping(path = "/{page}/{size}")
    public ResponseEntity<?> pagination(@PathVariable("page") int page, @PathVariable("size") int size) throws Exception {
        List<Product> product = productService.pagination(page, size);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PostMapping(path = "/add", consumes = "application/json")
    public ResponseEntity<?> addProduct(@RequestBody AddProductDto addProductDto, Authentication authentication) throws Exception {
        Category category = categoryService.getCategoryById(addProductDto.getCategoryId());
        Product products = productService.addProduct(authentication, addProductDto, category);
        return new ResponseEntity<>(products, HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") Long id, @RequestBody Product product, Authentication authentication) throws Exception {
        Product products = productService.updateProduct(id, product, authentication);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id, Authentication authentication) throws Exception {
        Product product = productService.deleteProduct(id, authentication);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping(path = "/search")
    public ResponseEntity<?> searchCart(@RequestParam("name") String name) {
        List<Product> products = productService.searchProduct(name);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
