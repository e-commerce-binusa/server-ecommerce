package com.ecommerce.binusa.controller;

import com.ecommerce.binusa.model.Category;
import com.ecommerce.binusa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping(path = "/all")
    public ResponseEntity<?> getAllCategory() {
        List<Category> categories = categoryService.getAllCategory();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping(path = "/id/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable("id") Long id) throws Exception {
        Category category = categoryService.getCategoryById(id);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @PostMapping(path = "/add", consumes = "application/json")
    public ResponseEntity<?> addCategory(@RequestBody Category category, Authentication authentication) throws Exception {
        Category category1 = categoryService.addCategory(category, authentication);
        return new ResponseEntity<>(category1, HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}", consumes = "application/json")
    public ResponseEntity<?> updateCategory(@PathVariable("id") Long id, @RequestBody Category category, Authentication authentication) throws Exception {
        Category category1 = categoryService.updateCategory(id, category, authentication);
        return new ResponseEntity<>(category1, HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Long id, Authentication authentication) throws Exception {
        Category category = categoryService.deleteCategory(id, authentication);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }
}
