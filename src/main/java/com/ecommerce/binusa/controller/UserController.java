package com.ecommerce.binusa.controller;

import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.payload.request.JwtRequest;
import com.ecommerce.binusa.payload.response.JwtResponse;
import com.ecommerce.binusa.repository.UserRepository;
import com.ecommerce.binusa.service.UserService;
import com.ecommerce.binusa.utill.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private void authenticate(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @GetMapping(path = "/role")
    public ResponseEntity<?> getAllUserRole() {
        List<User> user = userService.userRole();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<?> getAllUser() {
        List<User> user = userService.getAllUser();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(path = "/list/{id}")
    public ResponseEntity<?> userById(@PathVariable("id") Long id) {
        User user = userService.getUserId(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping(path = "/register", consumes = "application/json")
    public ResponseEntity<?> register(@RequestBody User user) throws Exception {
        User newUser = userService.createUser(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
        final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails);
        User user = userRepository.findByEmail(authenticationRequest.getEmail());
        return ResponseEntity.ok(new JwtResponse(token, user.getId(), user.getRole()));
    }

    @PutMapping(path = "/update/password", consumes = "application/json")
    public ResponseEntity<?> changePassword(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setUsername(userLoggedIn.getUsername());
        user.setName(userLoggedIn.getName());
        user.setEmail(userLoggedIn.getEmail());
        user.setPhoneNumber(userLoggedIn.getPhoneNumber());
        user.setImage(userLoggedIn.getImage());
        user.setRole(userLoggedIn.getRole());
        user.setAddress(userLoggedIn.getAddress());
        User userUpdate = userService.updatePassword(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

    @PutMapping(path = "/update/username")
    public ResponseEntity<?> changeUsername(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setName(userLoggedIn.getName());
        user.setPassword(userLoggedIn.getPassword());
        user.setEmail(userLoggedIn.getEmail());
        user.setPhoneNumber(userLoggedIn.getPhoneNumber());
        user.setImage(userLoggedIn.getImage());
        user.setRole(userLoggedIn.getRole());
        user.setAddress(userLoggedIn.getAddress());
        User userUpdate = userService.updateUsername(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

    @PutMapping(path = "/update/name")
    public ResponseEntity<?> changeName(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setUsername(userLoggedIn.getUsername());
        user.setEmail(userLoggedIn.getEmail());
        user.setPhoneNumber(userLoggedIn.getPhoneNumber());
        user.setImage(userLoggedIn.getImage());
        user.setAddress(userLoggedIn.getAddress());
        user.setRole(userLoggedIn.getRole());
        user.setPassword(userLoggedIn.getPassword());
        User userUpdate = userService.updateName(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

    @PutMapping(path = "/update/address")
    public ResponseEntity<?> changeAddress(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setName(userLoggedIn.getName());
        user.setUsername(userLoggedIn.getUsername());
        user.setEmail(userLoggedIn.getEmail());
        user.setPhoneNumber(userLoggedIn.getPhoneNumber());
        user.setImage(userLoggedIn.getImage());
        user.setRole(userLoggedIn.getRole());
        user.setPassword(userLoggedIn.getPassword());
        User userUpdate = userService.updateAddress(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

    @PutMapping(path = "/update/phoneNumber")
    public ResponseEntity<?> changePhoneNumber(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setUsername(userLoggedIn.getUsername());
        user.setName(userLoggedIn.getName());
        user.setEmail(userLoggedIn.getEmail());
        user.setAddress(userLoggedIn.getAddress());
        user.setImage(userLoggedIn.getImage());
        user.setRole(userLoggedIn.getRole());
        user.setPassword(userLoggedIn.getPassword());
        User userUpdate = userService.updatePhoneNumber(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

    @PutMapping(path = "/update/image")
    public ResponseEntity<?> changeImage(@RequestBody User user, Authentication authentication) throws Exception {
        User userLoggedIn = userRepository.findByEmail(authentication.getName());
        user.setId(userLoggedIn.getId());
        user.setUsername(userLoggedIn.getUsername());
        user.setAddress(userLoggedIn.getAddress());
        user.setName(userLoggedIn.getName());
        user.setPhoneNumber(userLoggedIn.getPhoneNumber());
        user.setEmail(userLoggedIn.getEmail());
        user.setRole(userLoggedIn.getRole());
        user.setPassword(userLoggedIn.getPassword());
        User userUpdate = userService.updateImage(user);
        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }

}
