package com.ecommerce.binusa.repository;

import com.ecommerce.binusa.model.Cart;
import com.ecommerce.binusa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    List<Cart> findAllByUser(User user);
    @Query("SELECT p FROM Cart p WHERE " +
            "p.product LIKE CONCAT('%',:product, '%') and p.user LIKE CONCAT('%',:user, '%')")
    List<Cart> findByUser(String product, String user);
    List<Cart> deleteByUser(User user);
}
