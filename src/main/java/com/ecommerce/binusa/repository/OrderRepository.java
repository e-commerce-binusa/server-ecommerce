package com.ecommerce.binusa.repository;

import com.ecommerce.binusa.model.Order;
import com.ecommerce.binusa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByUser(User user);
}
