package com.ecommerce.binusa.service;

import com.ecommerce.binusa.model.Order;
import com.ecommerce.binusa.model.Product;
import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.repository.OrderRepository;
import com.ecommerce.binusa.repository.ProductRepository;
import com.ecommerce.binusa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    public List<Order> addOrder(List<Order> orders, Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName());
        List<Product> products = new ArrayList<>();
        for (Order o : orders) {
            Product product = productRepository.findById(o.getProduct().getId()).orElse(null);
            o.setUser(user);
            o.setProduct(product);
            o.setProcess(false);
            o.setCreatedAt(new Date());
            product.setStock(product.getStock() - o.getTotalProduct());
            products.add(product);
        }
        productRepository.saveAll(products);
        return orderRepository.saveAll(orders);
    }

    public Order orderProcess(Long id) {
        Order orders = orderRepository.findById(id).orElse(null);
        orders.setProcess(true);
        return orderRepository.save(orders);
    }

    public List<Order> userOrder(Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName());
        return orderRepository.findByUser(user);
    }

    public List<Order> allOrder() {
        return orderRepository.findAll();
    }

    public Order orderId(Long id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            new Exception("Order not found!");
        }
        return order;
    }
}
