package com.ecommerce.binusa.service;

import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);

        List<SimpleGrantedAuthority> roles;
        if (user.getRole().equals("admin")) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
            return new org.springframework.security.core.userdetails.User(email, user.getPassword(), roles);
        }
        if (user.getRole().equals("user")) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
            return new org.springframework.security.core.userdetails.User(email, user.getPassword(), roles);
        }

        if (user == null) {
            throw new UsernameNotFoundException("User not found with email: " + email);
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                new ArrayList<>());
    }

    public User getUserId(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    private Boolean userExists(User user) {
        if (userRepository.findByUsername(user.getUsername()) != null) return true;
        if (userRepository.findByEmail(user.getEmail()) != null) return true;
        return false;
    }

    public User createUser(User user) throws Exception {
        String UserUsername = user.getUsername().trim();
        String UserEmail = user.getEmail().trim();
        String UserPassword = user.getPassword().trim();

        boolean UsernameIsNotvalid = (UserUsername == null) || !UserUsername.matches("[A-Za-z0-9_]+");
        boolean EmailIsNotValid = (UserEmail == null) || !UserEmail.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
        boolean PasswordIsNotValid = (UserPassword == null)
                || !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,20}");

        if (UsernameIsNotvalid) throw new Exception("Username not valid!");
        if (EmailIsNotValid) throw new Exception("Email not valid!");
        if (PasswordIsNotValid) throw new Exception("Password not valid!");
        if (userExists(user)) throw new Exception("User already exists!");

        String users = user.getRole();
        if (users == null) {
            user.setRole("user");
        } else {
            switch (user.getRole()) {
                case "admin":
                    user.setRole("admin");
                    break;
                default:
                    user.setRole("user");
                    break;
            }
        }
        if (user.getImage() == null) {
            user.setImage("https://firebasestorage.googleapis.com/v0/b/social-network-c2f4c.appspot.com/o/profile%2Fblank-profile-picture-ga42c45fd5_1280.png?alt=media&token=1dc72969-ac39-498f-aff0-2b32897395ae");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public List<User> userRole(){
        String role = "user";
        return userRepository.findByRole(role);
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public User updatePassword(User user) throws Exception {
        String UserPassword = user.getPassword().trim();
        boolean PasswordIsNotValid = (UserPassword == null)
                || !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new Exception("Password not valid!");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User updateUsername(User user) throws Exception {
        String UserUsername = user.getUsername().trim();
        boolean UsernameIsNotvalid = (UserUsername == null) || !UserUsername.matches("[A-Za-z0-9_]+");
        if (UsernameIsNotvalid) throw new Exception("Username not valid!");
        user.setUsername(user.getUsername());
        return userRepository.save(user);
    }

    public User updateName(User user) {
        user.setName(user.getName());
        return userRepository.save(user);
    }

    public User updateAddress(User user) {
        user.setAddress(user.getAddress());
        return userRepository.save(user);
    }

    public User updatePhoneNumber(User user) {
        user.setPhoneNumber(user.getPhoneNumber());
        return userRepository.save(user);
    }

    public User updateImage(User user) {
        user.setImage(user.getImage());
        return userRepository.save(user);
    }

}
