package com.ecommerce.binusa.service;

import com.ecommerce.binusa.model.Category;
import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.repository.CategoryRepository;
import com.ecommerce.binusa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserRepository userRepository;

    public List<Category> getAllCategory() {
        return categoryRepository.findAll();
    }

    public Category getCategoryById(Long id) throws Exception {
        Category category = categoryRepository.findById(id).orElse(null);
        if (category == null) throw new Exception("Category not found!!!");
        return category;
    }

    public Category addCategory(Category category, Authentication authentication) throws Exception {
        User user = userRepository.findByEmail(authentication.getName());
        category.setUser(user);
        return categoryRepository.save(category);
    }

    public Category updateCategory(Long id, Category category, Authentication authentication) throws Exception {
        Category category1 = categoryRepository.findById(id).orElse(null);
        User user = userRepository.findByEmail(authentication.getName());
        if (category1 == null) {
            throw new Exception("Category not found!!!");
        }
        if (!user.equals(category1.getUser())) {
            throw new Exception("No permission to update this category!!!");
        }
        category1.setName(category.getName());
        return categoryRepository.save(category1);
    }

    public Category deleteCategory(Long id, Authentication authentication) throws Exception {
        Category category = categoryRepository.findById(id).orElse(null);
        User user = userRepository.findByEmail(authentication.getName());
        if (category == null) {
            throw new Exception("Category not found!!!");
        }
        if (!user.equals(category.getUser())) {
            throw new Exception("No permission to delete this category!!!");
        }
        categoryRepository.delete(category);
        return category;
    }

}







