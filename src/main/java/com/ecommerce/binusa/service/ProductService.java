package com.ecommerce.binusa.service;

import com.ecommerce.binusa.dto.cartDto.AddToCartDto;
import com.ecommerce.binusa.dto.productDto.AddProductDto;
import com.ecommerce.binusa.model.Cart;
import com.ecommerce.binusa.model.Category;
import com.ecommerce.binusa.model.Product;
import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.repository.CategoryRepository;
import com.ecommerce.binusa.repository.ProductRepository;
import com.ecommerce.binusa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) throws Exception {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) throw new Exception("Product not found!!!");
        return product;
    }

    public Product addProduct(Authentication authentication, AddProductDto addProductDto, Category category) throws Exception {
        User user = userRepository.findByEmail(authentication.getName());
        Product products = new Product(
                addProductDto.getName(), addProductDto.getDescription(), addProductDto.getImage(),
                addProductDto.getPrice(), addProductDto.getStock(), category
        );
        products.setUser(user);
        return productRepository.save(products);
    }

    public Product updateProduct(Long id, Product product, Authentication authentication) throws Exception {
        User user = userRepository.findByEmail(authentication.getName());
        Product products = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new Exception("Product not found!!!");
        }
        if (!user.equals(products.getUser())) {
            throw new Exception("No permission to update this product!!!");
        }
        products.setName(product.getName());
        products.setDescription(product.getDescription());
        products.setImage(product.getImage());
        products.setPrice(product.getPrice());
        products.setStock(product.getStock());
        return productRepository.save(products);
    }

    public Product deleteProduct(Long id, Authentication authentication) throws Exception {
        Product product = productRepository.findById(id).orElse(null);
        User user = userRepository.findByEmail(authentication.getName());
        if (product == null) {
            throw new Exception("Product not found!!!");
        }
        if (!user.equals(product.getUser())) {
            throw new Exception("No permission to delete this product!!!");
        }
        productRepository.delete(product);
        return product;
    }

    public List<Product> pagination(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> products = productRepository.findAll(pageable);
        return products.toList();
    }

    public List<Product> searchProduct(String name) {
        return productRepository.findByName(name);
    }
}
