package com.ecommerce.binusa.service;

import com.ecommerce.binusa.dto.cartDto.AddToCartDto;
import com.ecommerce.binusa.dto.cartDto.CartDto;
import com.ecommerce.binusa.dto.cartDto.CartItemDto;
import com.ecommerce.binusa.model.Cart;
import com.ecommerce.binusa.model.Product;
import com.ecommerce.binusa.model.User;
import com.ecommerce.binusa.repository.CartRepository;
import com.ecommerce.binusa.repository.ProductRepository;
import com.ecommerce.binusa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CartService {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    public Cart addToCart(AddToCartDto addToCartDto, Product product, Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName());
        Cart cart = new Cart(addToCartDto.getQuantity(), product, user);
        return cartRepository.save(cart);
    }

    public CartDto listCart(Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName());
        List<Cart> cartList = cartRepository.findAllByUser(user);
        List<CartItemDto> cartItems = new ArrayList<>();
        CartDto cartDto = new CartDto();
        for (Cart cart : cartList) {
            CartItemDto cartItemDto = getDtoFromCart(cart);
            cartItems.add(cartItemDto);
        }
        double totalCost = 0;
        for (CartItemDto cartItemDto : cartItems) {
            totalCost += (cartItemDto.getProduct().getPrice() * cartItemDto.getQuantity());
        }
        int qty = 0;
        for (CartItemDto cartItemDto : cartItems) {
            qty += cartItemDto.getQuantity();
        }
        cartDto.setQuantity(qty);
        cartDto.setCartItems(cartItems);
        cartDto.setTotalCost(totalCost);
        return cartDto;
    }

    public List<Cart> getAll() {
        return cartRepository.findAll();
    }

    public Cart updateCart(Long id, Cart cart, Authentication authentication) throws Exception {
        Cart carts = cartRepository.findById(id).orElse(null);
        User user = userRepository.findByEmail(authentication.getName());
        if (carts == null) {
            throw new Exception("Cart id not found!!!");
        }
        if (!user.equals(carts.getUser())) {
            throw new Exception("No permission to update this cart!!!");
        }
        carts.setQuantity(cart.getQuantity());
        carts.setCreatedAt(new Date());
        return cartRepository.save(carts);
    }

    public List<Cart> searchCart(String product, String user) {
        return cartRepository.findByUser(product, user);
    }

    public Cart deleteCartItem(Long id, Authentication authentication) throws Exception {
        Cart cart = cartRepository.findById(id).orElse(null);
        User user = userRepository.findByEmail(authentication.getName());
        if (cart == null) {
            throw new Exception("Cart not found!!!");
        }
        if (!user.equals(cart.getUser())) {
            throw new Exception("No permission to delete this post!!!");
        }
        cartRepository.delete(cart);
        return cart;
    }

    @Transactional
    public List<Cart> deleteAllCart(Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName());
        return cartRepository.deleteByUser(user);
    }

    private static CartItemDto getDtoFromCart(Cart cart) {
        return new CartItemDto(cart);
    }
}
